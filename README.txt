Overview
========

BOM module seamlessly integrates BOM Widget into your site.

Features
========

  * Admin can input code, width and height of widget
  * Admin can select option to show logo in widget

Usage
========
  * Install the module
  * Access the configuration page and set widget code.
  * Access Blocks section and assign a location to module widget

Installation
============

See the included INSTALL.txt
